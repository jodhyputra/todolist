package com.binus;

public class Task {
    private int id;
    private String taskname;
    private Status status;

    public Task(int id, String taskname, Status status){
        this.id = id;
        this.taskname = taskname;
        this.status = status;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getTaskname() {
        return taskname;
    }
    public void setTaskname(String taskname) {
        this.taskname = taskname;
    }
    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }

    public String getTaskAttribute(Task task) {
        String task1 = task.getId() + ". " + task.getTaskname() + " [" + task.getStatus() + "]";
        return task1;
    }

    public static String changeStatus(String task){
        return task.replaceAll("NOT_DONE", "NOT DONE");
    }
}
