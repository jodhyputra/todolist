package com.binus;

import java.util.ArrayList;
import java.util.List;
import java.lang.String;
import java.util.stream.Collectors;

import static com.binus.Task.changeStatus;

public class TodoList {
    private List<Task> todoList;

    public void addTodoList(Task task){
        if(todoList == null){
            todoList = new ArrayList<>();
        }
        todoList.add(task);
    }

    public String getTodoList() {
        String resultTask = this.todoList.stream().map(
                task -> task.getId() + ". " + task.getTaskname() + " [" + task.getStatus() + "]"
        ).collect(Collectors.joining("\n"));
        String finalResultTask = changeStatus(resultTask);

        return finalResultTask;
    }
}
