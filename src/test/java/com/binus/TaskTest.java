package com.binus;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TaskTest {
    @Test
    public void testGetTask(){
        String expected = "1. Learn clean code [NOT_DONE]";
        Task task = new Task(1, "Learn clean code", Status.NOT_DONE);

        assertEquals(expected, task.getTaskAttribute(task));
    }
}
