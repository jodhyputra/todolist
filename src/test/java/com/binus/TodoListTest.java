package com.binus;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TodoListTest {
    @Test
    public void testGetTaskList(){
        String expected = "1. Do dishes [DONE]\n2. Learn java [NOT DONE]\n3. Learn TDD [NOT DONE]";
        Task task1 = new Task(1, "Do dishes", Status.DONE);
        Task task2 = new Task(2, "Learn java", Status.NOT_DONE);
        Task task3 = new Task(3, "Learn TDD", Status.NOT_DONE);

        TodoList todoList = new TodoList();
        todoList.addTodoList(task1);
        todoList.addTodoList(task2);
        todoList.addTodoList(task3);

        assertEquals(expected, todoList.getTodoList());
    }
}
